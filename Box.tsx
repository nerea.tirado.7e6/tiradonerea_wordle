import { BoxSatatus, Boxstatus } from "./types";

import styles from "./box.module.scss";
import className from "classnames/bind";

const classes = classNames.bind(styles);

interface BoxProps{
    value: string; 
    status: Boxstatus; 
}

export default function Box({value, status}: BoxProps) {

    const boxStatus = classes({
        correct:  status = "correct", 
        present: status = "present",
        absent: status = "absent",
        empty: staus = "empty",
        edit: status = "edit", 
    }); 
    return <div className={boxStatus}>{value}div>;
}