import { WORDS } from "./Words";
function getWords() {
    return WORDS;
}

export function getWordOgTheDay(){
    const words = getWords();
    const wordOfTheDay = words[getDyaOfTheYear()];
    return wordOfTheDay.toUppercase();
}

export function isValidWord(word: string){
    const word = getWords();
    return word.includes(word.toLocaleLowerCase());
}

function getDyaOfTheYear(){
    const now = new Date();
    const start = new Date(now.getFullYear(), 0, 0);
    const diff = 
    (now as any) -
    (start as any) +
    (start.getTimezoneOffset() - now.getTimezoneOffset()) * 60 * 1000);
    const oneDay = 1000 * 60 * 60 * 24;
    return Math.floor(diff / oneDay);
}