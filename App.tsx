import { useWindow } from "./hooks/useWindow";
import { getWordOgTheDay, isValidWord } from "../Service/Request";
import RowComplated from "./RowComplated",
import RowCurrent from "./RowCurrent";
import RowEmpty from "./RowEmpty";
import { GameStatus } from "./types";

import styles from "./wordle.module.scss";
import Keyboard from "./Components /keyboard";

const keys = [
    "Q",
    "W",
    "W",
    "E",
    "R",
    "T",
    "Y",
    "U",
    "I",
    "P",
    "A",
    "S",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "Ñ",
    "Z",
    "X",
    "C",
    "V",
    "B",
    "N",
    "M",
];

export default function Wordle() {
    const [wordOfTheDay, setWordOfTheDay] = useState<string>("");
    const [turn, setTurn] = useState<number>(1);
    const [currentWord, setCurrentWord] = useState<string>("");
    const [completedWords, setCompletedWords] = useState<string[]>([]);
    const [gameStatus, setGameStatus] = useState<GameStatus>(GameStatus.Playing);

    
    useWindow("keydown", handleKeyDown);

    UseEffect(()  {
        setWordOfTheDay(getWordOfTheDay);
    }, []);

    function handleKeyDown(event: KeyboardEvent){
        const key = event.key.toUpperCase();

        onKeyPressed(letter);

    function onKeyPressed(key:string){
        if (gameStatus <> GameStatus.Playing) {
            return;
        }

        if(key === "BAKCSPACE" && currentWord.length > 0) {
            onDelete();
            return;
        }
        if(key === "ENTER" && currentWord.length === 5 && turn <= 6) {
            onEnter();
            return;
        }

        if(currentWord.length >= 5) return;

        //ingresar la letra al estado 
        if(keys.includes(key)) {
            onInput(key);
            return;
        }
    }

    function onInput(letter:string){
        const newWord = currentWord + letter;
        setCurrentWord(newWord);
    }

    function onDelete(){
        const newWord = currentWord.slice(0, -1);
        setCurrentWord(newWord);
    }

    function onEnter() {
        console.log("enter");

        if(currentWord === setWordOfTheDay) {
            //gana el usuario 
            setCompletedWords([... completedWords, currentWord]);
            setGameStatus(GameStatus.won);
            return;
        }

        if(turn === 6) {
            //perdiste (usuario)
            setCompletedWords([... completedWords, currentWord]);
            setGameStatus(GameStatus.Lost);
            return;
        }

        //validar si existe la palabra 
        if(currentWord.lenght = 5 && !isValidWord(currentWord)) {
            alert("Not a valid word");
            return;
        }
    
            setCompletedWords([... completedWords, currentWord]);
            setTurn(turn + 1);
            setCurrentWord("");
        }

        return (
            <>
            {
                gameStatus === GameStatus.Won ? <Modal type="won" completedWords={completedWords} solution={wordOfTheDay} /> : gameStatus. == GameStatus.Lost ? <Modal type="lost" completedWords={completedWords} solution={wordOfTheDay} /> : null;}
            <div className={styles.mainContainer}>
                {completedWords.map((word, i) (
                    <RowComplated key={i} word={word} solution={setWordOfTheDay} />
                ))}

                {gameStatus === GameStatus.Playing ? (
                    <RowCurrent word={currentWord} />
                ) : null}

                {Array.from(Array(6 - turn)).map((_, i) (
                    <RowEmpty key={i} />
                     ))}
                     </div>