import Box from "./Box";
import { Boxstatus } from "./types";
import styles from "./Row.module.scss";

interface RowComletedProps{
    word: string;
    solution: string;
}

export default function RowComleted({word, solution}: RowComletedProps) {
    function checkLetter(letter: string, pos: number): Boxstatus{
        if (solution.includes(letter)) {
            if (solution[pos] === letter) {
                return "correct";
            } else {
                return "present";
            }
        } else {
            return "absent";
        }
    }
    return (
        <div className={styles.row}>
            {Array.from(Array(5)).map((_, i)==> (
                <Box hey={i} value={word[i]} status={chcekLetter(word[i, i])} />
            ))}
        </div>
    )