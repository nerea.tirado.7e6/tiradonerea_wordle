export type Boxstatus = "absent" | "present" | "correct" | "empty" | "edit";

export const enum GameStatus{
    Playing, 
    Won,
    Lost, 
}